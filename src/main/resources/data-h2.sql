insert into countries (co_country_name, co_country_code) values 
('United Kingdom', 'UK'),
('Germany', 'DE'),
('Poland', 'PL'),
('Croatia', 'HR');

insert into currencies (cu_currency_name, cu_currency_code, cu_country) values
('pound sterling', 'GBP', 1),
('euro', 'EUR', 2),
('polish zloty', 'PLN', 3),
('croatian kuna', 'HRK', 4);

insert into tax_information (ti_tax_percent, ti_fixed_costs, ti_country) values 
(25, 600, 1),
(20, 800, 2),
(19, 1200, 3);