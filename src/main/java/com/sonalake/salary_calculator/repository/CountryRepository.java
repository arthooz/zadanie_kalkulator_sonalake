package com.sonalake.salary_calculator.repository;

import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.Currency;
import com.sonalake.salary_calculator.domain.TaxInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interface for accessing currency/tax information stored in database
 *
 * @author Artur Kryszak
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByCountryCodeIgnoreCase(String cc);

    @Query("select c.taxInformation from Country c where upper(c.countryCode) = upper(:code)")
    TaxInformation findTaxInfoByCountryCode(@Param("code") String countryCode);

    @Query("select c from Country c join c.taxInformation ti where ti is not null order by c.countryName asc")
    List<Country> findAllWithTaxInfo();

    @Query("select c.currency from Country c where upper(c.countryCode) = upper(:code)")
    Currency findCurrencyByCountryCode(@Param("code") String countryCode);
}
