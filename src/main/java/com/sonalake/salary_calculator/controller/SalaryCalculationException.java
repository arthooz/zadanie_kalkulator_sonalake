package com.sonalake.salary_calculator.controller;

import org.springframework.http.HttpStatus;

class SalaryCalculationException extends RuntimeException {

    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public SalaryCalculationException(HttpStatus status, String message) {
        super(message);
        this.httpStatus = status;
    }

    public SalaryCalculationException(String message) {
        super(message);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
