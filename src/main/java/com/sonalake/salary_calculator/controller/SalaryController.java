package com.sonalake.salary_calculator.controller;

import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.TaxInformation;
import com.sonalake.salary_calculator.model.CalcData;
import com.sonalake.salary_calculator.model.SelectValue;
import com.sonalake.salary_calculator.service.ExchangeRateService;
import com.sonalake.salary_calculator.service.SalaryCalculationService;
import com.sonalake.salary_calculator.service.TaxInfoService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class SalaryController {

    private TaxInfoService taxInfoService;

    private ExchangeRateService exRateService;

    private SalaryCalculationService scService;

    @GetMapping(value = "/getSelectData", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SelectValue>> getSelectValues() {
        List<Country> countries = taxInfoService.getAllCountriesWithTaxInfo();
        if (countries.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        List<SelectValue> selectValues = countries.stream().map(this::convertToModel).collect(Collectors.toList());
        return new ResponseEntity<>(selectValues, HttpStatus.OK);
    }

    private SelectValue convertToModel(Country c) {
        return new SelectValue(c.getCountryName(), c.getCountryCode());
    }

    @PostMapping(value = "/calcSalary", produces = MediaType.TEXT_PLAIN_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> calculateSalary(@Valid @RequestBody CalcData cd) {
        TaxInformation taxInfo = taxInfoService.getTaxInfoByCountryCode(cd.getCountryCode());
        if (taxInfo == null)
            throw new SalaryCalculationException(HttpStatus.NOT_FOUND,
                    String.format("Tax information service for country code: %s not found", cd.getCountryCode()));

        String cCode = taxInfo.getCountry().getCurrency().getCurrencyCode();
        double exRate = exRateService.getExchangeRateForCurrency(cCode);
        if (Double.isNaN(exRate))
            throw new SalaryCalculationException(
                    String.format("Unable to get exchange rate for %s. Please try again later", cCode));

        double salary = scService.calculateSalary(taxInfo, exRate, cd.getDailyPay());
        if (Double.isNaN(salary))
            throw new SalaryCalculationException("Error occurred while calculating salary");

        return new ResponseEntity<>(String.format("%.2f", salary), HttpStatus.OK);
    }
}
