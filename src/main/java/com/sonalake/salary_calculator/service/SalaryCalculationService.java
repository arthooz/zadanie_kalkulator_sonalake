package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.domain.TaxInformation;

/**
 * Interface for services calculating salary.
 *
 * @author Artur Kryszak
 */
public interface SalaryCalculationService {
    /**
     * Calculates summary salary on contract lowered by tax information
     *
     * @param ti          tax information specific for country
     * @param currExRate  currency exchange rate
     * @param dailySalary daily net salary
     * @return Calculated salary or <code>NaN</code> if error will occur
     */
    double calculateSalary(TaxInformation ti, double currExRate, double dailySalary);
}
