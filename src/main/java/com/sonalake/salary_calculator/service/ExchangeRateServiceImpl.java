package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.service.datasource.CurrencyExchangeDataSource;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Service for getting currency exchange rates from external sources
 *
 * @author Artur Kryszak
 */
@Service
@Slf4j
@AllArgsConstructor
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private final CurrencyExchangeDataSource dataSource;

    @Override
    public double getExchangeRateForCurrency(String currencyCode) {
        if (StringUtils.length(currencyCode) != 3
                || !currencyCode.matches(CURRENCY_CODE_REGEX))
            throw new IllegalArgumentException(
                    "Currency code should be literal of length 3 without digits and any special characters");

        double cEr = dataSource.getCurrencyExchangeRate(currencyCode.toUpperCase());
        log.debug("Returned exchange rate: {}", cEr);

        if (Double.isNaN(cEr)) {
            log.debug("Data source returned null");
        }

        return cEr;
    }
}
