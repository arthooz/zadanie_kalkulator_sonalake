package com.sonalake.salary_calculator.service.datasource;

/**
 * Interface for exchange rates sources.
 *
 * @author Artur Kryszak
 */
public interface CurrencyExchangeDataSource {

    /**
     * Gets current currency exchange rate
     *
     * @param currencyCode valid three letter currency code - as stated in standard
     *                     <a href="http://www.xe.com/iso4217.php">ISO 4217</a>
     * @return Current exchange rate or <code>NaN</code> if error will occur
     */
    double getCurrencyExchangeRate(String currencyCode);
}
