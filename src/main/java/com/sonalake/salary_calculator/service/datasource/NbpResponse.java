package com.sonalake.salary_calculator.service.datasource;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class NbpResponse {
    private String table;
    private String currency;
    private String code;
    private List<Rate> rates;

    @Data
    @NoArgsConstructor
    public static class Rate {
        private String no;
        private String effectiveDate;
        private double mid;
    }
}
