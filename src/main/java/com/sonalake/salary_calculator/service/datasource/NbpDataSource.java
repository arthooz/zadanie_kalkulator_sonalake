package com.sonalake.salary_calculator.service.datasource;

import com.sonalake.salary_calculator.service.datasource.NbpResponse.Rate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

/**
 * Gets currency exchange info from NBP API
 *
 * @author Artur Kryszak
 */
@Component
@Slf4j
public class NbpDataSource implements CurrencyExchangeDataSource {

    private final RestTemplate restTemplate;
    private final String resourceUrl = "http://api.nbp.pl/api/exchangerates/rates/a/%s/";
    public static final String PLN_CODE = "PLN";

    public NbpDataSource(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Gets currency exchange rates from remote NBP API
     *
     * @param currencyCode valid three currency code
     * @return Current exchange rate or <code>NaN</code> if error will occur
     */
    @Override
    public double getCurrencyExchangeRate(String currencyCode) {
        if (currencyCode.equalsIgnoreCase(PLN_CODE)) {
            return 1;
        }

        String currResUrl = String.format(resourceUrl, currencyCode);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<NbpResponse> response = null;

        try {
            response = restTemplate.exchange(currResUrl, HttpMethod.GET, entity, NbpResponse.class);
        } catch (RestClientException e) {
            log.error("Error occurred while getting response from API", e);
        }

        if (response != null && response.getStatusCode().equals(HttpStatus.OK)) {
            NbpResponse resp = response.getBody();
            if (resp != null) {
                List<Rate> rates = resp.getRates();
                if (CollectionUtils.isNotEmpty(rates)) {
                    return rates.get(0).getMid();
                } else {
                    log.info("Exchange rates returned from API are empty");
                }
            }
        }

        return Double.NaN;
    }

}
