package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.TaxInformation;
import com.sonalake.salary_calculator.repository.CountryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for getting tax information based on country details
 *
 * @author Artur Kryszak
 */
@Service
@AllArgsConstructor
@Slf4j
public class TaxInfoServiceImpl implements TaxInfoService {

    private final CountryRepository countryRepo;

    @Override
    public TaxInformation getTaxInfoByCountryCode(String countryCode) {
        return countryRepo.findTaxInfoByCountryCode(countryCode);
    }

    @Override
    public List<Country> getAllCountriesWithTaxInfo() {
        List<Country> countries = countryRepo.findAllWithTaxInfo();

        List<Country> countriesList = ListUtils.emptyIfNull(countries);
        if (countriesList.isEmpty()) {
            log.debug("No data in repository");
        }
        return countriesList;
    }
}
