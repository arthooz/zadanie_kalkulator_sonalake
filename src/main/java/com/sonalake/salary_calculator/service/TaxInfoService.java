package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.TaxInformation;

import java.util.List;

/**
 * Interface for getting tax information based on country details
 *
 * @author Artur Kryszak
 */
public interface TaxInfoService {
    /**
     * Gets tax information for country
     *
     * @param countryCode valid country code as stated in in standard <a href="https://countrycode.org/">ISO 639</a>
     * @return country tax details or <code>null</code> when country will not be found
     */
    TaxInformation getTaxInfoByCountryCode(String countryCode);

    /**
     * Gets list of all supported countries containing tax information
     *
     * @return list of all supported countries with tax information
     */
    List<Country> getAllCountriesWithTaxInfo();
}
