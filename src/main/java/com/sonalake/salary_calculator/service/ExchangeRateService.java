package com.sonalake.salary_calculator.service;

/**
 * Interface for services returning currencies exchange rates
 *
 * @author Artur Kryszak
 */
public interface ExchangeRateService {
    String CURRENCY_CODE_REGEX = "^[a-zA-Z]{3}$";

    /**
     * Gets currency exchange rate
     *
     * @param currencyCode valid three letter currency code - as stated in standard
     *                     <a href="http://www.xe.com/iso4217.php">ISO 4217</a>
     * @return Current exchange rate or <code>NaN</code> if error will occur
     */
    double getExchangeRateForCurrency(String currencyCode);
}
