package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.domain.TaxInformation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SalaryCalculationServiceImpl implements SalaryCalculationService {

    @Value("${com.sonalake.salary_calculator.noOfDaysInMonth:22}")
    private int noOfDaysInMonth;

    @Override
    public double calculateSalary(TaxInformation ti, double currExRate, double dailySalary) {
        if (ti == null)
            throw new IllegalArgumentException("Tax information cannot be null");
        if (Double.isNaN(currExRate) || currExRate == 0)
            throw new IllegalArgumentException("Current exchange rate must be set");
        if (Double.isNaN(dailySalary) || dailySalary == 0)
            throw new IllegalArgumentException("Daily salary must be greater than 0");

        // compute net value
        double salary = dailySalary * noOfDaysInMonth;
        // Subtract fixed costs
        salary -= ti.getFixedCosts();
        // compute tax
        salary -= (ti.getTaxPercent() / 100.0) * salary;
        // convert currency
        salary *= currExRate;

        return salary;
    }

}
