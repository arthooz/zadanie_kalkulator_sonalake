package com.sonalake.salary_calculator.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Table for storing countries
 *
 * @author Artur Kryszak
 */
@Entity
@Table(name = "countries", indexes = {@Index(columnList = "co_country_code", name = "co_country_code_idx")})
@NoArgsConstructor
@Data
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "co_id", columnDefinition = "int auto_increment")
    private long id;

    @Column(name = "co_country_name", nullable = false, length = 100)
    private String countryName;

    @Column(name = "co_country_code", nullable = false, length = 2)
    private String countryCode;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "country")
    private TaxInformation taxInformation;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "country")
    private Currency currency;

    public Country(String cName, String cCode) {
        this.countryCode = cCode;
        this.countryName = cName;
    }
}
