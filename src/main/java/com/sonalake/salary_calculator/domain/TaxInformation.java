package com.sonalake.salary_calculator.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Table for storing tax information of countries
 *
 * @author Artur Kryszak
 */
@Entity
@Table(name = "tax_information")
@NoArgsConstructor
@Data
public class TaxInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "int auto_increment")
    private long id;

    @OneToOne(optional = false)
    @JoinColumn(name = "ti_country", unique = true, nullable = false, updatable = false)
    private Country country;

    @Column(name = "ti_tax_percent", nullable = false)
    private int taxPercent;

    @Column(name = "ti_fixed_costs", nullable = false)
    private int fixedCosts;
}
