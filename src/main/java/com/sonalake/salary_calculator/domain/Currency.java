package com.sonalake.salary_calculator.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Table for storing currencies information
 *
 * @author Artur Kryszak
 */
@Entity
@Table(name = "currencies")
@NoArgsConstructor
@Data
public class Currency {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cu_id", columnDefinition = "int auto_increment")
    private long id;

    @Column(name = "cu_currency_name", nullable = false, length = 100)
    private String currencyName;

    @Column(name = "cu_currency_code", nullable = false, length = 3)
    private String currencyCode;

    @OneToOne(optional = false)
    @JoinColumn(name = "cu_country", unique = true, nullable = false, updatable = false)
    private Country country;
}
