package com.sonalake.salary_calculator.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Model for incoming data from client to compute salary
 *
 * @author Artur Kryszak
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalcData {

    @NotNull(message = "Daily pay may not be null")
    private Double dailyPay;
    @NotNull(message = "Country code may not be null")
    @Pattern(regexp = "[a-zA-Z]{2}", message = "Country code should be valid ISO 639 code")
    private String countryCode;
}
