package com.sonalake.salary_calculator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Model for sending country select values to the client
 *
 * @author Artur Kryszak
 */
@AllArgsConstructor
@Data
public class SelectValue {

    private String label;
    private String value;
}
