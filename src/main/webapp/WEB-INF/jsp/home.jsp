<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Salary calculator</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <link rel="stylesheet" href="static/css/styles.css">
</head>
<body>
<div class="container salary-calc">
    <div class="col-md-6 col-md-offset-3 col-calc">
        <form id="calc-form" method="post">
            <fieldset>
                <legend class="sr-only">Salary calculator</legend>
                <div class="form-group">
                    <div class="input-group">
                        <input type="number" class="form-control" name="dailyPay" id="dailyPay" aria-label="Daily pay in project" required />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="btn-text">Choose country</span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" id="countries-dropdown">
                            </ul>
                        </div>
                        <input type="hidden" name="countryCode" id="countryCode" value="" />
                    </div>
                </div>
            </fieldset>
            <button type="button" class="btn btn-calc btn-primary pull-right" name="calc">Calculate</button>
        </form>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="static/js/scripts.js"></script>
</body>
</html>