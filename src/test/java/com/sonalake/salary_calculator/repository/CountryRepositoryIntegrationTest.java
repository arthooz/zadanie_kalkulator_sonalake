package com.sonalake.salary_calculator.repository;

import com.sonalake.salary_calculator.IntegrationTest;
import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.Currency;
import com.sonalake.salary_calculator.domain.TaxInformation;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql(scripts = "classpath:data-h2.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Category(IntegrationTest.class)
public class CountryRepositoryIntegrationTest {

    @Autowired
    private CountryRepository countryRepo;

    @Test
    public void findCountryByCountryCode() {
        Country ctry = countryRepo.findByCountryCodeIgnoreCase("pl");

        assertThat(ctry).isNotNull();
    }

    @Test
    public void findAllCountries() {
        List<Country> findAll = countryRepo.findAll();

        assertThat(findAll.size()).isEqualTo(4);
    }

    @Test
    public void findCurrencyByCountryCode() {
        Currency curr = countryRepo.findCurrencyByCountryCode("de");

        assertThat(curr).isNotNull();
    }

    @Test
    public void findCurrencyNameByCountryCode() {
        Currency curr = countryRepo.findCurrencyByCountryCode("DE");

        assertThat(curr.getCurrencyName()).isEqualTo("euro");
    }

    @Test
    public void findTaxInfoByCountryCode() {
        TaxInformation ti = countryRepo.findTaxInfoByCountryCode("pl");

        assertThat(ti.getTaxPercent()).isEqualTo(19);
    }

    @Test
    public void findCountriesWithTaxInfo() {
        List<Country> countries = countryRepo.findAllWithTaxInfo();

        assertThat(countries).hasSize(3);
    }
}
