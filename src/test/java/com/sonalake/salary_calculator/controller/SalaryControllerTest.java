package com.sonalake.salary_calculator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.Currency;
import com.sonalake.salary_calculator.domain.TaxInformation;
import com.sonalake.salary_calculator.model.CalcData;
import com.sonalake.salary_calculator.model.SelectValue;
import com.sonalake.salary_calculator.service.ExchangeRateService;
import com.sonalake.salary_calculator.service.SalaryCalculationService;
import com.sonalake.salary_calculator.service.TaxInfoService;
import lombok.Data;
import lombok.Getter;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SalaryController.class)
public class SalaryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objMap;

    @MockBean
    private TaxInfoService taxInfoService;

    @MockBean
    private ExchangeRateService exRateService;

    @MockBean
    private SalaryCalculationService scService;

    @Test
    public void fetchSelectValuesWithData() throws Exception {
        Country cnty = new Country("Poland", "PL");
        List<Country> list = Collections.singletonList(cnty);

        when(taxInfoService.getAllCountriesWithTaxInfo()).thenReturn(list);

        SelectValue sv = new SelectValue(cnty.getCountryName(), cnty.getCountryCode());

        // @formatter:off
        mockMvc.perform(get("/api/getSelectData").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].label", is(sv.getLabel())));
        // @formatter:on
    }

    @Test
    public void fetchSelectValuesWithoutData() throws Exception {
        when(taxInfoService.getAllCountriesWithTaxInfo()).thenReturn(Lists.emptyList());

        // @formatter:off
        mockMvc.perform(get("/api/getSelectData").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        // @formatter:on
    }

    @Test
    public void calcSalaryForUnknownCountry() throws Exception {
        when(taxInfoService.getTaxInfoByCountryCode(anyString())).thenReturn(null);

        CalcData cd = new CalcData(80.0, "AU");

        // @formatter:off
        mockMvc.perform(
                post("/api/calcSalary")
                        .content(toJson(cd))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        // @formatter:on
    }

    @Test
    public void calcSalaryForTooLongCountryCode() throws Exception {
        when(taxInfoService.getTaxInfoByCountryCode(anyString())).thenReturn(null);

        CalcData cd = new CalcData(80.0, "AUS");

        // @formatter:off
        mockMvc.perform(
                post("/api/calcSalary")
                        .content(toJson(cd))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        // @formatter:on
    }

    @Test
    public void calcSalaryForDecimalCountryCode() throws Exception {
        when(taxInfoService.getTaxInfoByCountryCode(anyString())).thenReturn(null);

        CalcData cd = new CalcData(80.0, "12");

        // @formatter:off
        mockMvc.perform(
                post("/api/calcSalary")
                        .content(toJson(cd))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        // @formatter:on
    }

    @Test
    public void calcSalaryForDailyPayNotNumber() throws Exception {
        EvilCalcData cd = new EvilCalcData("AAA", "AUS");

        // @formatter:off
        mockMvc.perform(
                post("/api/calcSalary")
                        .content(toJson(cd))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        // @formatter:on
    }

    @Test
    public void calcSalaryForNullCalcData() throws Exception {
        // @formatter:off
        mockMvc.perform(
                post("/api/calcSalary")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        // @formatter:on
    }

    @Test
    public void calcSalaryWithoutExchangeCourse() throws Exception {
        TaxInformation ti = new TaxInformation();
        Currency cu = new Currency();
        cu.setCurrencyCode("EUR");
        cu.setCurrencyName("euro");
        Country c = new Country("Germany", "DE");
        c.setCurrency(cu);
        ti.setCountry(c);
        when(taxInfoService.getTaxInfoByCountryCode(anyString())).thenReturn(ti);
        when(exRateService.getExchangeRateForCurrency(cu.getCurrencyCode())).thenReturn(Double.NaN);

        CalcData cd = new CalcData(80.0, "DE");

        // @formatter:off
        mockMvc.perform(
                post("/api/calcSalary")
                        .content(toJson(cd))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
        // @formatter:on
    }

    @Test
    public void calcSalaryWithCorrectData() throws Exception {
        double exRate = 4.5;
        double dailyPay = 80.0;
        String cc = "DE";

        TaxInformation ti = new TaxInformation();
        Currency cu = new Currency();
        cu.setCurrencyCode("EUR");
        cu.setCurrencyName("euro");
        Country c = new Country("Germany", cc);
        c.setCurrency(cu);
        ti.setCountry(c);

        when(taxInfoService.getTaxInfoByCountryCode(anyString())).thenReturn(ti);
        when(exRateService.getExchangeRateForCurrency(cu.getCurrencyCode())).thenReturn(exRate);
        when(scService.calculateSalary(ti, exRate, dailyPay)).thenReturn(500.0);

        CalcData cd = new CalcData(dailyPay, cc);

        // @formatter:off
        mockMvc.perform(
                post("/api/calcSalary")
                        .content(toJson(cd))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        // @formatter:on
    }

    private String toJson(Object o) throws Exception {
        return objMap.writeValueAsString(o);
    }

    @Getter
    static class EvilCalcData {

        private String dailyPay;
        private String countryCode;

        EvilCalcData(String dailyPay, String countryCode) {
            this.dailyPay = dailyPay;
            this.countryCode = countryCode;
        }
    }
}
