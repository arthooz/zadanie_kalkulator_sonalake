package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.service.datasource.CurrencyExchangeDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ExchangeRateServiceImplTest {

    @TestConfiguration
    static class TaxInfoServiceImplTestContextConfiguration {

        @MockBean
        private CurrencyExchangeDataSource exDataSource;

        @Bean
        public ExchangeRateService exchangeRateService() {
            return new ExchangeRateServiceImpl(exDataSource);
        }
    }

    @Autowired
    private ExchangeRateService exRateService;

    @Autowired
    private CurrencyExchangeDataSource exDataSource;

    @Before
    public void setUp() {
        when(exDataSource.getCurrencyExchangeRate("pln")).thenReturn(4.5);
        when(exDataSource.getCurrencyExchangeRate("PLN")).thenReturn(4.5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkExceptionWithNullArgument() {
        String cc = null;
        exRateService.getExchangeRateForCurrency(cc);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkExceptionWithEmptyArgument() {
        String cc = "";
        exRateService.getExchangeRateForCurrency(cc);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkExceptionWithTooShortArgument() {
        String cc = "pl";
        exRateService.getExchangeRateForCurrency(cc);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkExceptionWithTooLongArgument() {
        String cc = "plplpl";
        exRateService.getExchangeRateForCurrency(cc);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkExceptionWithDecimalArgument() {
        String cc = "123";
        exRateService.getExchangeRateForCurrency(cc);
    }

    @Test
    public void checkResultWithCorrectLowerCaseArgument() {
        String cc = "pln";
        double exRate = exRateService.getExchangeRateForCurrency(cc);

        assertThat(exRate).isEqualTo(4.5);
    }

    @Test
    public void checkResultWithCorrectUpperCaseArgument() {
        String cc = "PLN";
        double exRate = exRateService.getExchangeRateForCurrency(cc);

        assertThat(exRate).isEqualTo(4.5);
    }
}
