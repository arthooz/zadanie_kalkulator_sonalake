package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.Application;
import com.sonalake.salary_calculator.IntegrationTest;
import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.TaxInformation;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Sql(scripts = "classpath:data-h2.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
@Category(IntegrationTest.class)
public class TaxInfoServiceImplIntegrationTest {

    @Autowired
    private TaxInfoService taxService;

    @Test
    public void findTaxInfoByCountryCode() {
        String code = "pl";
        TaxInformation found = taxService.getTaxInfoByCountryCode(code);

        assertThat(found.getFixedCosts()).isEqualTo(1200);
    }

    @Test
    public void findTaxInfoByCountryCodeUppercase() {
        String code = "PL";
        TaxInformation found = taxService.getTaxInfoByCountryCode(code);

        assertThat(found.getTaxPercent()).isEqualTo(19);
    }

    @Test
    public void findAllCountriesWithTaxInfo() {
        List<Country> countries = taxService.getAllCountriesWithTaxInfo();

        assertThat(countries).hasSize(3);
    }
}
