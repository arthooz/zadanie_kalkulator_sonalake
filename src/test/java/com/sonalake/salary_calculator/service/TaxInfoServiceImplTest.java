package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.domain.Country;
import com.sonalake.salary_calculator.domain.TaxInformation;
import com.sonalake.salary_calculator.repository.CountryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class TaxInfoServiceImplTest {

    @TestConfiguration
    static class TaxInfoServiceImplTestContextConfiguration {

        @MockBean
        private CountryRepository countryRepo;

        @Bean
        public TaxInfoService taxInfoService() {
            return new TaxInfoServiceImpl(countryRepo);
        }
    }

    @Autowired
    private TaxInfoService taxService;

    @Autowired
    private CountryRepository countryRepo;

    @Before
    public void setUp() {
        Country pol = new Country();
        pol.setCountryCode("pl");
        pol.setCountryName("Poland");
        TaxInformation ti = new TaxInformation();
        ti.setFixedCosts(1200);
        ti.setTaxPercent(19);
        pol.setTaxInformation(ti);

        when(countryRepo.findTaxInfoByCountryCode(pol.getCountryCode())).thenReturn(ti);
        when(countryRepo.findTaxInfoByCountryCode(pol.getCountryCode().toUpperCase())).thenReturn(ti);
    }

    @Test
    public void findTaxInfoByCountryCode() {
        String code = "pl";
        TaxInformation found = taxService.getTaxInfoByCountryCode(code);

        assertThat(found.getFixedCosts()).isEqualTo(1200);
    }

    @Test
    public void findTaxInfoByCountryCodeUppercase() {
        String code = "PL";
        TaxInformation found = taxService.getTaxInfoByCountryCode(code);

        assertThat(found.getTaxPercent()).isEqualTo(19);
    }

    @Test
    public void findAllCountriesWithTaxInfoAndNullReturned() {
        when(countryRepo.findAllWithTaxInfo()).thenReturn(null);
        List<Country> countries = taxService.getAllCountriesWithTaxInfo();

        assertThat(countries).hasSize(0);
    }

    @Test
    public void findAllCountriesWithTaxInfoAndNotEmptyListReturned() {
        when(countryRepo.findAllWithTaxInfo()).thenReturn(Collections.singletonList(new Country()));
        List<Country> countries = taxService.getAllCountriesWithTaxInfo();

        assertThat(countries).hasSize(1);
    }
}
