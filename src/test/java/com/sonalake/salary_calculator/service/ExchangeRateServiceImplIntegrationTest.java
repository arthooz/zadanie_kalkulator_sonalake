package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.Application;
import com.sonalake.salary_calculator.IntegrationTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
@Category(IntegrationTest.class)
public class ExchangeRateServiceImplIntegrationTest {

    @Autowired
    private ExchangeRateService exRateService;

    @Test
    public void checkResultWithLowercaseNonSupportedCurrency() {
        String cc = "yyy";
        double exRate = exRateService.getExchangeRateForCurrency(cc);

        assertThat(exRate).isNaN();
    }

    @Test
    public void checkResultWithUppercaseNonSupportedCurrency() {
        String cc = "YYY";
        double exRate = exRateService.getExchangeRateForCurrency(cc);

        assertThat(exRate).isNaN();
    }

    @Test
    public void checkResultWithPlnCurrency() {
        String cc = "PLN";
        double exRate = exRateService.getExchangeRateForCurrency(cc);

        assertThat(exRate).isEqualTo(1);
    }

    @Test
    public void checkResultWithLowercaseSupportedCurrency() {
        String cc = "eur";
        double exRate = exRateService.getExchangeRateForCurrency(cc);

        assertThat(exRate).isNotNaN();
    }

    @Test
    public void checkResultWithUppercaseSupportedCurrency() {
        String cc = "EUR";
        double exRate = exRateService.getExchangeRateForCurrency(cc);

        assertThat(exRate).isNotNaN();
    }
}
