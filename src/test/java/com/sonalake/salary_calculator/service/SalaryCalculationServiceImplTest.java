package com.sonalake.salary_calculator.service;

import com.sonalake.salary_calculator.domain.TaxInformation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class SalaryCalculationServiceImplTest {

    @TestConfiguration
    static class SalaryCalculationServiceImplTestConfig {

        @Bean
        public SalaryCalculationService getSalaryCalcService() {
            return new SalaryCalculationServiceImpl();
        }
    }

    @Autowired
    private SalaryCalculationService salaryCalcService;

    @Test(expected = IllegalArgumentException.class)
    public void checkForNullTaxInfo() {
        salaryCalcService.calculateSalary(null, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkForNanExRate() {
        TaxInformation tx = new TaxInformation();
        salaryCalcService.calculateSalary(tx, Double.NaN, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkForZeroExRate() {
        TaxInformation tx = new TaxInformation();
        salaryCalcService.calculateSalary(tx, 0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkForNanDailyPay() {
        TaxInformation tx = new TaxInformation();
        salaryCalcService.calculateSalary(tx, 1, Double.NaN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkForZeroDailyPay() {
        TaxInformation tx = new TaxInformation();
        salaryCalcService.calculateSalary(tx, 1, 0);
    }

    @Test
    public void checkForZeroFixedCosts() {
        TaxInformation tx = new TaxInformation();
        tx.setFixedCosts(0);
        tx.setTaxPercent(50);

        double salary = salaryCalcService.calculateSalary(tx, 1, 1);

        assertThat(salary).isEqualTo(11);
    }

    @Test
    public void checkForZeroTaxPercent() {
        TaxInformation tx = new TaxInformation();
        tx.setFixedCosts(0);
        tx.setTaxPercent(0);

        double salary = salaryCalcService.calculateSalary(tx, 1, 1);

        assertThat(salary).isEqualTo(22);
    }

    @Test
    public void checkForRealExRate() {
        TaxInformation tx = new TaxInformation();
        tx.setFixedCosts(10);
        tx.setTaxPercent(20);

        double salary = salaryCalcService.calculateSalary(tx, 2, 20);

        assertThat(salary).isEqualTo(688);
    }
}
