package com.sonalake.salary_calculator.service.datasource;

import com.sonalake.salary_calculator.service.datasource.NbpResponse.Rate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class NbpDataSourceTest {

    @TestConfiguration
    static class NbpDataSourceTestContextConfiguration {
        @MockBean
        private RestTemplate restTemplate;

        @Bean
        public CurrencyExchangeDataSource exDataSource() {
            return new NbpDataSource(restTemplate);
        }
    }

    @Autowired
    private CurrencyExchangeDataSource exDataSource;

    @Autowired
    private RestTemplate restTemplate;

    @Before
    public void setUp() {
        NbpResponse mock = new NbpResponse();
        Rate rate = new Rate();
        rate.setMid(4.0);
        mock.setRates(Collections.singletonList(rate));

        ResponseEntity<NbpResponse> response = new ResponseEntity<>(mock, HttpStatus.OK);

        when(restTemplate.exchange(endsWith("eur/"), eq(HttpMethod.GET),
                any(HttpEntity.class), same(NbpResponse.class))).thenReturn(response);

        response = new ResponseEntity<>(mock, HttpStatus.NOT_FOUND);

        when(restTemplate.exchange(endsWith("123/"), eq(HttpMethod.GET),
                any(HttpEntity.class), same(NbpResponse.class))).thenReturn(response);
    }

    @Test
    public void checkRateForExistingCurrency() {
        String curr = "eur";
        double rate = exDataSource.getCurrencyExchangeRate(curr);

        assertThat(rate).isNotNaN();
    }

    @Test
    public void checkExactRateForExistingCurrency() {
        String curr = "eur";
        double rate = exDataSource.getCurrencyExchangeRate(curr);

        assertThat(rate).isEqualTo(4.0);
    }

    @Test
    public void checkNullForNonExistingCurrency() {
        String curr = "123";
        double rate = exDataSource.getCurrencyExchangeRate(curr);

        assertThat(rate).isNaN();
    }

    @Test
    public void checkExactRateForOurCurrency() {
        String curr = "pln";
        double rate = exDataSource.getCurrencyExchangeRate(curr);

        assertThat(rate).isEqualTo(1.0);
    }
}
