package com.sonalake.salary_calculator.service.datasource;

import com.sonalake.salary_calculator.Application;
import com.sonalake.salary_calculator.IntegrationTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
@Category(IntegrationTest.class)
public class NbpDataSourceIntegrationTest {

    @Autowired
    private CurrencyExchangeDataSource exDataSource;

    @Test
    public void getRateForExistingCurrency() {
        String curr = "eur";
        double exchangeRate = exDataSource.getCurrencyExchangeRate(curr);

        assertThat(exchangeRate).isNotNaN();
    }

    @Test
    public void getRateForNonExistingCurrency() {
        String curr = "123";
        double exchangeRate = exDataSource.getCurrencyExchangeRate(curr);

        assertThat(exchangeRate).isNaN();
    }
}
