let gulp = require('gulp');
let plugins = require('gulp-load-plugins')({
    DEBUG: false
});

// source and distribution folder
let src = './';
let dest = '../main/webapp/static/',
    scripts = 'js/',
    styles = 'css/';


// SASS options
let scss = {
	in: src + 'scss/**/*.scss',
    out: dest + styles,
    watch: src + 'scss/**/*.scss',
    sassOpts: {
        outputStyle: 'nested',
        precision: 3,
        errLogToConsole: true,
        includePaths: []
    }
};

gulp.task('styles', function() {
    let processors = [
        plugins.autoprefixer
    ];

    return gulp.src(scss.in)
        .pipe(plugins.sass(scss.sassOpts).on('error', plugins.sass.logError))
        .pipe(plugins.postcss(function() {
            return processors;
        }))
        .pipe(plugins.util.env.production ? plugins.cssnano() : plugins.util.noop())
        .pipe(gulp.dest(scss.out));
});

gulp.task('scripts', function() {
    return gulp.src(src + scripts + '*.js')
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('default'))
        .pipe(plugins.util.env.production ? plugins.uglify() : plugins.util.noop())
        .on('error', function (err) { plugins.util.log(plugins.util.colors.red('[Error]'), err.toString()); })
        .pipe(!plugins.util.env.production ? plugins.sourcemaps.init({
            loadMaps: true
        }) : plugins.util.noop())
        .pipe(!plugins.util.env.production ? plugins.sourcemaps.write('./maps') : plugins.util.noop())
        .pipe(gulp.dest(dest + scripts));
});

// copy bootstrap required fonts to dest
gulp.task('fonts', function() {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});


gulp.task('build', ['styles', 'scripts'], function() {
  // build it
});

//Watch task
gulp.task('default', ['styles', 'scripts'], function() {
    gulp.watch(scss.watch, ['styles']);
    gulp.watch(src + scripts + '*.js', ['scripts']);
});
