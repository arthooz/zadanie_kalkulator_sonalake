$(document).ready(
		function() {
			var methods = {
				GET : "GET",
				POST : "POST"
			};
			
			var alertTypes = {
					INFO : "success",
					ERROR : "danger"
			};
			
			var serverCall = function(url, method, dataObj, successCallback) {
				var dataStr = '';
				if (dataObj) {
					dataStr = JSON.stringify(dataObj);
				}
				
				var parseResponseError = function(response) {
					var text = response.responseText;
					
					try {
						text = JSON.parse(response.responseText);
					} catch(e) {} 	
					
					if (text.error)
						return text.error;
					else
						return text;
				};
				
				$.ajax(url, {
					type : method,
					data : (method == methods.POST) ? dataStr : dataObj,
					contentType: 'application/json',
					statusCode : {
						400 : function(response) {
							displayAlert('Server returned error: ' + parseResponseError(response), alertTypes.ERROR);
						},
						404 : function(response) {
							displayAlert('Server returned error: ' + parseResponseError(response), alertTypes.ERROR);									
						},
						415 : function(response) {
							displayAlert('Server returned error: ' + parseResponseError(response), alertTypes.ERROR);								
						}
					},
					success : function(fromServer) {
						successCallback(fromServer);
					},
				});
			};

			var serializeFormData = function($form) {
				var jq_serialize = $form.serializeArray();
				var form_serialize = {};

				$.map(jq_serialize, function(n, i) {
					form_serialize[n.name] = n.value;
				});

				return form_serialize;
			};
			
			var displayAlert = function(content, type) {
				var $alert = $('.col-calc .alert');
				if ($alert.length > 0) {
					$alert.slideDown('slow');
					$alert.remove();
				}
				
				$('.col-calc').prepend('<div class="alert alert-'+type+'" role="alert">'+content+'</div>');
			};

			serverCall('api/getSelectData', methods.GET, {}, function(data) {
				if (data instanceof Array) {
					$dropd = $('#countries-dropdown');
					$dropd.empty();
					$.each(data, function(i, item) {
						$dropd.append('<li><a href="#" data-value="' + item.value + '">' + item.label + '</a></li>');
					});
				}
			});

			$('#countries-dropdown').on('click', 'a', (function(e) {
				e.preventDefault();
				$('#countryCode').val($(this).data('value'));
				$('.dropdown-toggle .btn-text').text($(this).text());
			}));

			$('.btn-calc').click(function(e) {
				e.preventDefault();
				if ($('#dailyPay').val().length === 0 || $('#countryCode').val().length === 0) {
					$('#calc-form .form-group').addClass('has-error');					
					displayAlert('Enter daily pay and select country', alertTypes.ERROR);
					return false;
				}
				
				$('#calc-form .form-group').removeClass('has-error');
				$('#calc-form input').prop('readonly', true);
				
				serverCall('api/calcSalary', methods.POST, serializeFormData($('#calc-form')), function(data) {
					displayAlert('Computed salary: ' + data, alertTypes.INFO);
				});
				
				$('#calc-form input').prop('readonly', false);
			});
		});