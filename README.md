# Salary calculator

## Building frontend
    cd src/frontend
    npm install
    gulp build

## Running calculator
    ./mvnw spring-boot:run -Dspring.profiles.active=dev
Application can be accessed under `http://localhost:8080/` [open it](http://localhost:8080/).